﻿using Core.LocalStorage;
using Core.Repositories;
using Core.Services;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using TinkoffNews.Services;

namespace TinkoffNews.ViewModels
{
    public class Locator
    {
        public Locator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            SimpleIoc.Default.Register<INavigationService, NavigationService>();
            SimpleIoc.Default.Register<IConnectivityService, ConnectivityService>();
            SimpleIoc.Default.Register<ILocalStorage, LocalStorage>();
            SimpleIoc.Default.Register<ICacheRepository, CacheRepository>();

            SimpleIoc.Default.Register<NewsViewModel>();
            SimpleIoc.Default.Register<ContentViewModel>();
        }

        public NewsViewModel NewsPage => ServiceLocator.Current.GetInstance<NewsViewModel>();
        public ContentViewModel ContentPage => ServiceLocator.Current.GetInstance<ContentViewModel>();
    }
}
