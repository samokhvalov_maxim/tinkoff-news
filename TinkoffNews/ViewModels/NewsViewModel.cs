﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Popups;
using Core.Services;
using Localization.Resources;
using TinkoffNews.Converters;
using TinkoffNews.Models;

namespace TinkoffNews.ViewModels
{
    public class NewsViewModel : BaseViewModel
    {
        private readonly IConnectivityService _connectivityService;

        public NewsViewModel(IConnectivityService connectivityService)
        {
            _connectivityService = connectivityService;
        }

        #region Properties

        public string NewsHeaderText => AppResources.GetString("NewsHeader");

        private ObservableCollection<NewsItem> _news;
        public ObservableCollection<NewsItem> News
        {
            get
            {
                return _news;               
            }
            set
            {
                if (_news == value)
                    return;

                _news = value;
                RaisePropertyChanged();
            }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                if (_isLoading == value)
                    return;

                _isLoading = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        protected override void PageOpened()
        {
            if (IsBack)
                return;

            LoadNews();
        }

        public async void LoadNews()
        {
            try
            {
                IsLoading = true;

                var response = await _connectivityService.GetNewsAsync();
                if (response.Error != null || response.ResultCode != "OK")
                {
                    await new MessageDialog(AppResources.GetString("NewsLoadErrorText"), AppResources.GetString("ErrorText")).ShowAsync();
                    return;
                }

                var newsList = new List<NewsItem>();
               
                foreach (var news in response.Payload)
                {
                    var item = new NewsItem
                    {
                        Id = news.Id,
                        Header = StringCutter.DecodeAndCleanString(news.Text),
                        PublishDate = news.PublicationDate.Milliseconds
                    };

                    newsList.Add(item);
                }

                newsList = newsList.OrderByDescending(o => o.PublishDate).ToList();
                News = new ObservableCollection<NewsItem>(newsList);
            }
            finally
            {
                IsLoading = false;
            }
        }
    }
}
