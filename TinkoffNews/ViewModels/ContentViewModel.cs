﻿using System;
using Windows.UI.Popups;
using Core.Services;
using Localization.Resources;
using TinkoffNews.Converters;
using TinkoffNews.Models;
using TinkoffNews.Services;

namespace TinkoffNews.ViewModels
{
    public class ContentViewModel : BaseViewModel
    {
        private const string CssStyle = "<style>html { -ms-text-size-adjust:auto;  }  ul { padding:0;} " 
            + "body{background:white;color:black;font-family:'Segoe UI';font-size:14pt;margin:0;padding:0;display: block;}}</style>";
        //For correct viewing on win10 mobile (more here: https://social.msdn.microsoft.com/Forums/en-US/3963b8fe-1e12-4895-9645-105d04994418/uwpchtml-webview-displays-text-smaller-in-li-and-table-html-tags?forum=wpdevelop)

        private readonly IConnectivityService _connectivityService;
        private readonly INavigationService _navigationService;

        public ContentViewModel(IConnectivityService connectivityService, INavigationService navigationService)
        {
            _connectivityService = connectivityService;
            _navigationService = navigationService;
        }

        #region Properties

        private string _contentHeader;
        public string ContentHeader
        {
            get { return _contentHeader; }
            set
            {
                if (_contentHeader == value)
                    return;

                _contentHeader = value;
                RaisePropertyChanged();
            }
        }

        private string _content;
        public string Content
        {
            get { return _content; }
            set
            {
                if (_content == value)
                    return;

                _content = value;
                RaisePropertyChanged();
            }
        }

        private bool _isLoading;
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                if (_isLoading == value)
                    return;

                _isLoading = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        protected override void PageOpened()
        {
            var pageInData = _navigationService.GetPageInData<ContentPageInData>();

            if (pageInData == null)
                return;

            Content = "";
            ContentHeader = "";

            LoadContent(pageInData.Id);
        }

        private async void LoadContent(string id)
        {
            try
            {
                IsLoading = true;

                var response = await _connectivityService.GetContentAsync(id);
                if (response.Error != null || response.ResultCode != "OK")
                {
                    await new MessageDialog(AppResources.GetString("ContentLoadErrorText"), AppResources.GetString("ErrorText")).ShowAsync();
                    return;
                }

                ContentHeader = StringCutter.DecodeAndCleanString(response.Payload.Title.Text);
                Content = CssStyle + StringCutter.DecodeAndCleanString(response.Payload.Content);
            }
            finally
            {
                IsLoading = false;
            }
        }
    }
}
