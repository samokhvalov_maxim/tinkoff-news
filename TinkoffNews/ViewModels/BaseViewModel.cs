﻿using GalaSoft.MvvmLight;

namespace TinkoffNews.ViewModels
{
    public class BaseViewModel : ViewModelBase
    {
        public bool IsBack { get; set; }

        public void OnPageOpened(bool isBack)
        {
            IsBack = isBack;
            PageOpened();
        }

        public void OnPageClosed(bool isBack)
        {
            IsBack = isBack;
            PageClosed();
        }

        protected virtual void PageOpened() { }
        protected virtual void PageClosed() { }
    }
}
