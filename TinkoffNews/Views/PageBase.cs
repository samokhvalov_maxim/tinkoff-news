﻿using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using TinkoffNews.ViewModels;

namespace TinkoffNews.Views
{
    public class PageBase : Page
    {
        private BaseViewModel _viewModel;

        public PageBase()
        {
            DataContextChanged += OnDataContextChanged;
        }

        private void OnDataContextChanged(FrameworkElement sender, DataContextChangedEventArgs args)
        {
            _viewModel = DataContext as BaseViewModel;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _viewModel.OnPageOpened(e.NavigationMode == NavigationMode.Back);

            CheckAndSetBackButton();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            _viewModel.OnPageClosed(e.NavigationMode == NavigationMode.Back);
        }

        private void CheckAndSetBackButton()
        {
            var rootFrame = Window.Current.Content as Frame;

            if (rootFrame == null)
                return;

            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = rootFrame.CanGoBack
                ? AppViewBackButtonVisibility.Visible
                : AppViewBackButtonVisibility.Collapsed;
        }
    }
}
