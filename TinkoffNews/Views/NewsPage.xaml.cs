﻿using TinkoffNews.Controls;
using TinkoffNews.ViewModels;

namespace TinkoffNews.Views
{
    public sealed partial class NewsPage
    {
        public NewsPage()
        {
            InitializeComponent();
        }

        private void ListView_OnRefreshRequested(object sender, RefreshRequestedEventArgs e)
        {
            var vm = DataContext as NewsViewModel;
            vm?.LoadNews();
        }

        private void listView_PullProgressChanged(object sender, RefreshProgressEventArgs e)
        {
            if (e.IsRefreshable)
            {
                if (e.PullProgress == 1)
                {
                    // Progress = 1.0 means that the refresh has been triggered.
                    if (SpinnerStoryboard.GetCurrentState() == Windows.UI.Xaml.Media.Animation.ClockState.Stopped)
                    {
                        SpinnerStoryboard.Begin();
                    }
                }
                else if (SpinnerStoryboard.GetCurrentState() != Windows.UI.Xaml.Media.Animation.ClockState.Stopped)
                {
                    SpinnerStoryboard.Stop();
                }
                else
                {
                    // Turn the indicator by an amount proportional to the pull progress.
                    SpinnerTransform.Angle = e.PullProgress * 360;
                }
            }
        }
    }
}
