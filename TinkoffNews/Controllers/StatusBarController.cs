﻿using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Core;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Core.Controllers;
using Localization.Resources;

namespace TinkoffNews.Controllers
{
    public class StatusBarController
    {
        private StatusBar _statusBar;

        private static StatusBarController _instance;
        public static StatusBarController Instance => _instance ?? (_instance = new StatusBarController());

        private StatusBarController()
        {

        }

        public async void Init()
        {
            if (!Windows.Foundation.Metadata.ApiInformation.IsTypePresent("Windows.UI.ViewManagement.StatusBar"))
                return;

            _statusBar = StatusBar.GetForCurrentView();
            NetworkController.Instance.StatusChanged += UpdateStatusBar;

            await Task.Delay(500);
            UpdateStatusBar(NetworkController.Instance.IsInternet);
        }

        private async void UpdateStatusBar(bool isInternet)
        {
            if (isInternet)
            {
                await HideRedPanel();
            }
            else
            {
                await ShowRedPanel(AppResources.GetString("NoInternetMessage"));
            }
        }

        private async Task ShowRedPanel(string text)
        {
            if (_statusBar == null)
                return;

            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                _statusBar.BackgroundColor = (Color) Application.Current.Resources["RedColor"];
                _statusBar.ForegroundColor = Colors.White;
                _statusBar.ProgressIndicator.Text = text;
                _statusBar.BackgroundOpacity = 1.0;

                await _statusBar.ProgressIndicator.ShowAsync();
                _statusBar.ProgressIndicator.ProgressValue = 0;
            });
        }

        private async Task HideRedPanel()
        {
            if (_statusBar == null)
                return;

            await CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                _statusBar.BackgroundColor = Colors.White;
                _statusBar.ForegroundColor = Colors.Black;
                _statusBar.ProgressIndicator.Text = "";
                _statusBar.BackgroundOpacity = 1.0;

                await _statusBar.ProgressIndicator.HideAsync();
            });
        }
    }
}
