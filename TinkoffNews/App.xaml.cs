﻿using System;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Core;
using Core.Controllers;
using Core.LocalStorage;
using GalaSoft.MvvmLight.Ioc;
using Localization.Resources;
using TinkoffNews.Controllers;
using TinkoffNews.Services;
using TinkoffNews.Views;

namespace TinkoffNews
{
    sealed partial class App
    {
        public App()
        {
            InitializeComponent();
            Suspending += OnSuspending;
            AppResources.Init();
            NetworkController.Instance.Init();
        }

        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {            
            SystemNavigationManager.GetForCurrentView().BackRequested += OnBackRequested;
            StatusBarController.Instance.Init();

            var rootFrame = Window.Current.Content as Frame;

            if (rootFrame == null)
            {
                rootFrame = new Frame();
                rootFrame.NavigationFailed += OnNavigationFailed;

                Window.Current.Content = rootFrame;
            }

            if (e.PrelaunchActivated == false)
            {
                if (rootFrame.Content == null)
                {
                    rootFrame.Navigate(typeof(NewsPage), e.Arguments);
                }

                Window.Current.Activate();
            }
          
            SimpleIoc.Default.GetInstance<ILocalStorage>().Initialize();
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (e.Handled)
                return;

            e.Handled = true;
            SimpleIoc.Default.GetInstance<INavigationService>().GoBack();
        }

        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            deferral.Complete();
        }
    }
}
