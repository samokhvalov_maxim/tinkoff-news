﻿using System;

namespace TinkoffNews.Services
{
    public interface INavigationService
    {
        void NavigateTo(Type sourcePageType);
        void NavigateTo<TPageInData>(Type sourcePageType, TPageInData parameters) where TPageInData : class;
        void GoBack();
        TPageInData GetPageInData<TPageInData>();
    }
}
