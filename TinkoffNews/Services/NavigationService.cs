﻿using System;
using System.Collections.Generic;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using GalaSoft.MvvmLight.Ioc;

namespace TinkoffNews.Services
{
    public class NavigationService : INavigationService
    {
        public void NavigateTo(Type sourcePageType)
        {
            ((Frame)Window.Current.Content).Navigate(sourcePageType);
        }

        public void NavigateTo<TPageInData>(Type sourcePageType, TPageInData parameters) where TPageInData : class
        {            
            if (!SimpleIoc.Default.IsRegistered<Stack<TPageInData>>())
            {
                SimpleIoc.Default.Register(() => new Stack<TPageInData>());
            }

            var stack = SimpleIoc.Default.GetInstance<Stack<TPageInData>>();
            stack.Push(parameters);

            NavigateTo(sourcePageType);
        }

        public void GoBack()
        {
            var content = (Frame) Window.Current.Content;
            if (content.CanGoBack)
            {
                content.GoBack();
            }
            else
            {
                Application.Current.Exit();
            }
        }

        public TPageInData GetPageInData<TPageInData>()
        {
            if (!SimpleIoc.Default.IsRegistered<Stack<TPageInData>>())
            {
                SimpleIoc.Default.Register(() => new Stack<TPageInData>());
            }

            var stack = SimpleIoc.Default.GetInstance<Stack<TPageInData>>();
            if (stack.Count > 0)
            {
                return stack.Peek();
            }

            return default(TPageInData);
        }
    }
}
