﻿using System.Globalization;
using System.Net;
using System.Text.RegularExpressions;

namespace TinkoffNews.Converters
{
    public static class StringCutter
    {
        private static string DecodeEncodedNonAsciiCharacters(string value)
        {
            return Regex.Replace(
                value,
                @"\\u(?<Value>[a-zA-Z0-9]{4})",
                m => ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString());
        }

        public static string DecodeAndCleanString(string value)
        {
            string result = WebUtility.HtmlDecode(value);
            result = DecodeEncodedNonAsciiCharacters(result);
            result = result.Replace("</nobr>", "").Replace("<nobr>", "");
            return result.Replace("</p>", "").Replace("<p>", "");
        }      
    }
}
