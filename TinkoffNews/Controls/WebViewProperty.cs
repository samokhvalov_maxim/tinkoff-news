﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace TinkoffNews.Controls
{
    public class WebViewProperty
    {
        public static readonly DependencyProperty HtmlStringProperty =
           DependencyProperty.RegisterAttached("HtmlString", typeof(string), typeof(WebViewProperty), new PropertyMetadata("", OnHtmlStringChanged));

        public static string GetHtmlString(DependencyObject obj) { return (string)obj.GetValue(HtmlStringProperty); }
        public static void SetHtmlString(DependencyObject obj, string value) { obj.SetValue(HtmlStringProperty, value); }

        private static void OnHtmlStringChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var wv = d as WebView;
            wv?.NavigateToString((string)e.NewValue);
        }
    }
}
