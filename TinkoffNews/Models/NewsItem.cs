﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using TinkoffNews.Services;
using TinkoffNews.Views;

namespace TinkoffNews.Models
{
    public class NewsItem
    {
        public string Id { get; set; }
        public string Header { get; set; }
        public long PublishDate { get; set; }

        public RelayCommand OpenNewsCommand => new RelayCommand(OpenNews);

        private void OpenNews()
        {
            var pageInData = new ContentPageInData() {Id = Id};
            SimpleIoc.Default.GetInstance<INavigationService>().NavigateTo(typeof(ContentPage), pageInData);
        }
    }
}
