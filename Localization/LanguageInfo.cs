﻿using Windows.System.UserProfile;

namespace Localization
{
    public static class LanguageInfo
    {
        public static string GetFullCurrentLang()
        {
            var currentLang = GlobalizationPreferences.Languages[0];
            switch (currentLang)
            {
                case "ru": return "ru-RU";
                case "en-US": return "en-US";
                default: return "ru-RU";
            }
        }
    }
}
