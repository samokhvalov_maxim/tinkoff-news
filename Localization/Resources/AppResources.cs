﻿using Windows.ApplicationModel.Resources;
using Windows.Globalization;

namespace Localization.Resources
{
    public static class AppResources
    {
        private static ResourceLoader _resources;

        public static void Init()
        {
            ApplicationLanguages.PrimaryLanguageOverride = LanguageInfo.GetFullCurrentLang();
            _resources = new ResourceLoader("/Localization/Resources");
        }

        public static string GetString(string key)
        {
            return _resources.GetString(key);
        }
    }
}
