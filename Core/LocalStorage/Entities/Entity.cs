﻿using SQLite;

namespace Core.LocalStorage.Entities
{
    public class Entity
    {
        [PrimaryKey]
        public string Id { get; set; }
        public int Type { get; set; }
        public string Data { get; set; }
    }
}
