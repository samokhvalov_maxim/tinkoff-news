﻿using System.Collections.Generic;
using System.Linq;
using Core.LocalStorage.SQLite;

namespace Core.LocalStorage.Entities
{
    public class EntityStorage
    {
        #region Fields

        private static readonly Dictionary<TypeId, Entity> MemoryCache = new Dictionary<TypeId, Entity>();
        private static readonly object Lock = new object();

        #endregion

        #region Initialize

        public void Initialize()
        {
            SQLiteStorage.Instance.Initialize();
        }

        #endregion

        #region Public Methods

        public void Put(string id, EntityType type, string data)
        {
            lock (Lock)
            {
                var entity = SQLiteStorage.Instance.GetEntityFromStorage(id, type);

                if (entity != null)
                {
                    entity.Type = type.ToInt();
                    entity.Data = data;

                    SQLiteStorage.Instance.UpdateEntityInStorage(entity);
                }
                else
                {
                    entity = new Entity
                    {
                        Id = id,
                        Type = type.ToInt(),
                        Data = data
                    };

                    SQLiteStorage.Instance.InsertEntityToStorage(entity);
                }

                PutToMemory(entity);
            }
        }

        public Entity Get(string id, EntityType type)
        {
            lock (Lock)
            {
                {
                    var typeId = new TypeId { Id = id, Type = type.ToInt() };

                    if (MemoryCache.ContainsKey(typeId)) return MemoryCache[typeId];
                }

                return PutToMemory(SQLiteStorage.Instance.GetEntityFromStorage(id, type));
            }
        }

        public List<Entity> GetAll(EntityType type)
        {
            lock (Lock)
            {
                return SQLiteStorage.Instance.GetEntitiesFromStorage(type);
            }
        }

        public void Delete(string id, EntityType type)
        {
            lock (Lock)
            {
                SQLiteStorage.Instance.DeleteEntityFromStorage(id, type);
                DeleteFromMemory(id, type.ToInt());
            }
        }

        public void DeleteAll(EntityType type)
        {
            lock (Lock)
            {
                SQLiteStorage.Instance.DeleteAllEntitiesFromStorage(type);
                DeleteFromMemory(type.ToInt());
            }
        }

        public void Clear()
        {
            SQLiteStorage.Instance.ClearStorage();
        }

        #endregion

        #region MemoryCache Methods

        #region TypeId

        private struct TypeId
        {
            public string Id;
            public int Type;

            public override int GetHashCode()
            {
                return Id.GetHashCode() ^ Type.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                if (!(obj is TypeId)) return false;
                return ((TypeId)obj).Id == Id && ((TypeId)obj).Type == Type;
            }
        }

        #endregion

        private static Entity PutToMemory(Entity entity)
        {
            lock (Lock)
            {
                if (entity == null) return null;

                var typeId = new TypeId { Id = entity.Id, Type = entity.Type };
                MemoryCache[typeId] = entity;

                return entity;
            }
        }

        private static void DeleteFromMemory(string id, int type)
        {
            lock (Lock)
            {
                var delme = MemoryCache.Where(item => item.Value.Id == id && item.Value.Type == type).ToList();

                foreach (var item in delme)
                {
                    MemoryCache.Remove(item.Key);
                }
            }
        }

        private static void DeleteFromMemory(int type)
        {
            lock (Lock)
            {
                var delme = MemoryCache.Where(item => item.Key.Type == type).ToList();

                foreach (var item in delme)
                {
                    MemoryCache.Remove(item.Key);
                }
            }
        }

        #endregion
    }
}
