﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Core.LocalStorage.Entities;
using SQLite;

namespace Core.LocalStorage.SQLite
{
    public class SQLiteStorage : SQLiteConnection
    {
        #region Constructor

        private SQLiteStorage(string databasePath) : base(databasePath)
        {
            Execute("PRAGMA temp_store_directory = '" + DbInfo.GetDbFolder() + "';");
        }

        #endregion

        #region Singleton

        private static SQLiteStorage _instance;

        public static SQLiteStorage Instance => _instance ?? CreateNewInstance();

        private static SQLiteStorage CreateNewInstance()
        {
            return _instance = new SQLiteStorage(DbInfo.GetDbPath());
        }

        #endregion

        #region Initialization

        public bool Initialize()
        {
            try
            {
                CreateTable<Entity>();
                
                return true;
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Initialize Storage exception: {0}", exception.Message);
                return false;
            }
        }

        #endregion

        #region ClearStorage

        public void ClearStorage()
        {
            try
            {
                Execute("vacuum;");
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Clear Storage exception: " + exception.Message);
            }
        }

        #endregion

        #region Public Methods

        public Entity GetEntityFromStorage(string id, EntityType type)
        {
            return GetEntityFromStorage(id, type.ToInt());
        }

        public List<Entity> GetEntitiesFromStorage(EntityType type)
        {
            return GetEntitiesFromStorage(type.ToInt());
        }

        public void InsertEntityToStorage(Entity cachedEntity)
        {
            Insert(cachedEntity);
        }

        public void UpdateEntityInStorage(Entity cachedEntity)
        {
            Update(cachedEntity);
        }

        public void DeleteEntityFromStorage(string id, EntityType type)
        {
            DeleteEntityFromStorage(id, type.ToInt());
        }

        public void DeleteAllEntitiesFromStorage(EntityType type)
        {
            DeleteAllEntitiesFromStorage(type.ToInt());
        }

        #endregion

        #region Private Methods

        private Entity GetEntityFromStorage(string id, int type)
        {
            return Table<Entity>().Where(x => x.Id == id && x.Type == type).Take(1).FirstOrDefault();
        }

        private List<Entity> GetEntitiesFromStorage(int type)
        {
            return Table<Entity>().Where(x => x.Type == type).ToList();
        }

        private void DeleteEntityFromStorage(string id, int type)
        {
            var entity = Table<Entity>().Where(x => x.Id == id && x.Type == type).Take(1).FirstOrDefault();

            if (entity == null) return;

            Delete(entity);
        }

        private void DeleteAllEntitiesFromStorage(int type)
        {
            var entities = Table<Entity>().Where(x => x.Type == type);

            if (entities == null) return;

            foreach (var entity in entities)
            {
                Delete(entity);
            }
        }

        #endregion
    }
}