﻿using System;
using Windows.Storage;

namespace Core.LocalStorage.SQLite
{
    public static class DbInfo
    {
        public const string DbFolder = "Db";
        public const string DbName = "TinkoffNews";
        public const string DbExtension = "db";

        public static string FullDbName => DbName + "." + DbExtension;

        public static string GetDbPath()
        {
            try
            {
                var dbFolderPath = GetDbFolder();

                return dbFolderPath + "\\" + FullDbName;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string GetDbFolder()
        {
            try
            {
                var task = ApplicationData.Current.LocalFolder.CreateFolderAsync(DbFolder,
                    CreationCollisionOption.OpenIfExists);

                task.AsTask().Wait();

                var dbFolder = task.GetResults();

                return dbFolder.Path;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
