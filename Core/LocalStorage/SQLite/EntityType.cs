﻿namespace Core.LocalStorage.SQLite
{
    public enum EntityType
    {
        Unknown,
        Cache
    }

    public static class EntityTypeConverter
    {
        public static int ToInt<T>(this T value) where T : struct
        {
            object temp = value;
            return (int)temp;
        }
    }
}
