﻿using System.Collections.Generic;
using Core.LocalStorage.SQLite;

namespace Core.LocalStorage
{
    public interface ILocalStorage
    {
        void Initialize();
        void Clear();
        bool Put<TData>(string key, EntityType type, TData data);
        TData Get<TData>(string key, EntityType type);
        List<TData> GetAll<TData>(EntityType type);
        void Delete(string key, EntityType type);
        void DeleteAll(EntityType type);
    }
}
