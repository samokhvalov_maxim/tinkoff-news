﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Core.LocalStorage.Entities;
using Core.LocalStorage.SQLite;
using Newtonsoft.Json;

namespace Core.LocalStorage
{
    public class LocalStorage : ILocalStorage
    {       
        private readonly EntityStorage _storage;

        public LocalStorage()
        {
            _storage = new EntityStorage();
        }

        public void Initialize()
        {
            _storage.Initialize();
        }

        public void Clear()
        {
            _storage.Clear();
        }

        public bool Put<TData>(string key, EntityType type, TData data)
        {
            try
            {
                _storage.Put(key, type, Serialize(data));
                return true;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(string.Format("Exception: {0}", exception.Message));
                return false;
            }
        }

        public TData Get<TData>(string key, EntityType type)
        {
            var entity = _storage.Get(key, type);
            if (entity == null) return default(TData);

            return Deserialize<TData>(entity.Data);
        }

        public List<TData> GetAll<TData>(EntityType type)
        {
            var entities = _storage.GetAll(type);
            if (entities == null)
                return new List<TData>();

            return entities.Select(entity => Deserialize<TData>(entity.Data)).ToList();
        }

        public void Delete(string key, EntityType type)
        {
            _storage.Delete(key, type);
        }

        public void DeleteAll(EntityType type)
        {
            _storage.DeleteAll(type);
        }

        protected static string Serialize<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        protected static T Deserialize<T>(string xml)
        {
            return JsonConvert.DeserializeObject<T>(xml);
        }
    }
}
