﻿using System;
using Windows.Networking.Connectivity;

namespace Core.Controllers
{
    public class NetworkController
    {     
        private static NetworkController _instance;
        public static NetworkController Instance => _instance ?? (_instance = new NetworkController());

        private NetworkController()
        {

        }

        public event Action<bool> StatusChanged;
        public bool IsInternet { get; set; }

        public void Init()
        {
            NetworkInformation.NetworkStatusChanged += NetworkInformationOnNetworkStatusChanged;
            IsInternet = CheckIsInternet();
        }

        private bool CheckIsInternet()
        {
            var connections = NetworkInformation.GetInternetConnectionProfile();
            var internet = connections != null && connections.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
            return internet;
        }

        private void NetworkInformationOnNetworkStatusChanged(object sender)
        {
            IsInternet = CheckIsInternet();
            StatusChanged?.Invoke(IsInternet);
        }
    }
}
