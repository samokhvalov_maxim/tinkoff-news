﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Core.Controllers;
using Core.DataContracts;
using Core.Repositories;
using Core.Rest;
using Core.Rest.DataContracts;
using Core.Rest.Exceptions;

namespace Core.Services
{
    public class ConnectivityService : IConnectivityService
    {
        private const string ApiHost = "https://api.tinkoff.ru/v1/";
        private readonly ICacheRepository _cacheRepository;

        public ConnectivityService(ICacheRepository cacheRepository)
        {
            _cacheRepository = cacheRepository;
        }
               
        public async Task<NewsResponse> GetNewsAsync()
        {
            var url = ApiHost + "news";
            var request = new NetHttpRequest<NewsResponse>(url, HttpMethod.Get);
            return await GetResponse(request);
        }

        public async Task<ContentResponse> GetContentAsync(string id)
        {
            var url = ApiHost + "news_content?id=" + id;
            var request = new NetHttpRequest<ContentResponse>(url, HttpMethod.Get);
            return await GetResponse(request);
        }

        private async Task<TData> GetResponse<TData>(IBaseRequest<TData> request) where TData : BaseResponse, new()
        {
            try
            {
                if (!NetworkController.Instance.IsInternet)
                {
                    var cachedResult = _cacheRepository.GetResponse<TData>(request.GetUrl());
                    if (cachedResult == null)
                        throw new NetworkException();

                    return cachedResult;
                }

                var result = await request.SendAsync();
                if (result.Error == null)
                {
                    _cacheRepository.Add(request.GetUrl(), result);
                }

                return result;
            }
            catch (Exception ex)
            {
                return new TData { Error = ex };
            }
        }
    }
}
