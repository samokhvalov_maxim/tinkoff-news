﻿using System.Threading.Tasks;
using Core.Rest.DataContracts;

namespace Core.Services
{
    public interface IConnectivityService
    {
        Task<NewsResponse> GetNewsAsync();
        Task<ContentResponse> GetContentAsync(string id);
    }
}
