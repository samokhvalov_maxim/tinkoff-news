﻿using System;
using System.Threading.Tasks;
using Core.DataContracts;

namespace Core.Rest
{
    public interface IBaseRequest<TDataContract> : IDisposable where TDataContract : BaseResponse, new()
    {
        Task<TDataContract> SendAsync();
        string GetUrl();
    }
}
