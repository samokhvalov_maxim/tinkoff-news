﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Core.Controllers;
using Core.DataContracts;
using Core.Rest.Exceptions;
using Newtonsoft.Json;

namespace Core.Rest
{
    public class NetHttpRequest<TDataContract> : IBaseRequest<TDataContract> where TDataContract : BaseResponse, new()
    {
        private readonly string _url;
        private readonly HttpMethod _method;
        private readonly HttpClient _client;

        protected CancellationTokenSource CancelToken;
       
        public NetHttpRequest(string url, HttpMethod method)
        {
            _url = url;
            _method = method;

            var handler = new HttpClientHandler();
            if (handler.SupportsAutomaticDecompression)
            {
                handler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            }

            var client = new HttpClient(handler) { Timeout = TimeSpan.FromSeconds(1000) };
            CancelToken = new CancellationTokenSource();

            _client = client;
        }

        public async Task<TDataContract> SendAsync()
        {
            HttpResponseMessage response = await BaseSendAsync();
            string responseString = await GetResponseString(response);
            responseString = responseString.Replace("\n", "").Replace("\t", "");
            return DeserializeData(responseString);
        }

        public string GetUrl()
        {
            return _url;           
        }

        public void Dispose()
        {
            CancelRequest();
            _client.Dispose();
        }

        private async Task<HttpResponseMessage> BaseSendAsync()
        {
            var request = new HttpRequestMessage
            {
                Method = _method,
                RequestUri = new Uri(_url)
            };

            return await BaseSendAsync(request);
        }

        private async Task<HttpResponseMessage> BaseSendAsync(HttpRequestMessage request)
        {
            if (!NetworkController.Instance.IsInternet)
                throw new NetworkException();

            try
            {
                return await _client.SendAsync(request, CancelToken.Token);
            }
            catch (Exception)
            {
                throw new Exception("Send async exception");
            }
        }

        private async Task<string> GetResponseString(HttpResponseMessage response)
        {
            var responseString = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return responseString;
            }

            throw new Exception(response.StatusCode + responseString);
        }

        private TDataContract DeserializeData(string responseString)
        {
            try
            {
                return JsonConvert.DeserializeObject<TDataContract>(responseString);
            }
            catch (Exception ex)
            {
                throw new Exception("DeserializeException: Response: " + responseString + " Message: " + ex.Message);
            }
        }

        private void CancelRequest()
        {
            if (!CancelToken.Token.CanBeCanceled) return;

            try
            {
                CancelToken.Cancel();
            }
            catch (Exception)
            {

            }
        }
    }
}
