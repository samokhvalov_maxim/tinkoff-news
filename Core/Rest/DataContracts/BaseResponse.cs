﻿using System;
using Newtonsoft.Json;

namespace Core.DataContracts
{
    public class BaseResponse
    {
        [JsonIgnore]
        public Exception Error;
    }
}
