﻿using System.Collections.Generic;
using Core.DataContracts;
using Newtonsoft.Json;

namespace Core.Rest.DataContracts
{
    public class NewsResponse : BaseResponse
    {
        [JsonProperty("resultCode")]
        public string ResultCode { get; set; }

        [JsonProperty("payload")]
        public List<NewsPayload> Payload { get; set; }
    }

    public class NewsPayload
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("publicationDate")]
        public NewsDate PublicationDate { get; set; }
    }

    public class NewsDate
    {
        [JsonProperty("milliseconds")]
        public long Milliseconds { get; set; }
    }
}
