﻿using Core.DataContracts;
using Newtonsoft.Json;

namespace Core.Rest.DataContracts
{
    public class ContentResponse : BaseResponse
    {
        [JsonProperty("resultCode")]
        public string ResultCode { get; set; }

        [JsonProperty("payload")]
        public ContentPayload Payload { get; set; }
    }

    public class ContentPayload
    {
        [JsonProperty("title")]
        public NewsPayload Title { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }
    }
}
