﻿using Core.LocalStorage;
using Core.LocalStorage.SQLite;

namespace Core.Repositories
{
    public class CacheRepository : AbstractRepository<string, object>, ICacheRepository
    {  
        public CacheRepository(ILocalStorage localStorage) : base(localStorage)
        {
        }

        protected override EntityType Type => EntityType.Cache;

        protected override string GenerateKey(string parameters)
        {
            return "url-" + parameters;
        }

        public T GetResponse<T>(string parameters)
        {
            return LocalStorage.Get<T>(GenerateKey(parameters), EntityType.Cache);
        }
    }
}
