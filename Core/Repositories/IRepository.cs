﻿using System.Collections.Generic;

namespace Core.Repositories
{
    public interface IRepository<in TParams, TData>
    {
        void Add(TParams parameters, TData data);
        TData Get(TParams parameters);
        List<TData> GetAll();
        void Delete(TParams parameters);
        void DeleteAll();
    }
}
