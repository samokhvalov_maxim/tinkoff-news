﻿using System.Collections.Generic;
using Core.LocalStorage;
using Core.LocalStorage.SQLite;

namespace Core.Repositories
{
    public abstract class AbstractRepository<TParams, TData> : IRepository<TParams, TData>
    {      
        protected readonly ILocalStorage LocalStorage;

        protected AbstractRepository(ILocalStorage localStorage)
        {
            LocalStorage = localStorage;
        }

        public void Add(TParams parameters, TData data)
        {
            LocalStorage.Put(GenerateKey(parameters), Type, data);
        }

        public TData Get(TParams parameters)
        {
            return LocalStorage.Get<TData>(GenerateKey(parameters), Type);
        }

        public T Get<T>(TParams parameters)
        {
            return LocalStorage.Get<T>(GenerateKey(parameters), Type);
        }

        public List<TData> GetAll()
        {
            return LocalStorage.GetAll<TData>(Type);
        }

        public void Delete(TParams parameters)
        {
            LocalStorage.Delete(GenerateKey(parameters), Type);
        }

        public void DeleteAll()
        {
            LocalStorage.DeleteAll(Type);
        }

        protected abstract EntityType Type { get; }
        protected abstract string GenerateKey(TParams parameters);
    }
}
