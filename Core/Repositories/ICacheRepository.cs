﻿namespace Core.Repositories
{
    public interface ICacheRepository : IRepository<string, object>
    {
        T GetResponse<T>(string v);
    }
}
